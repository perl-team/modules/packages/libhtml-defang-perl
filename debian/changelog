libhtml-defang-perl (1.07-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper dependency to >= 9, since that's what is used in
    debian/compat.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Update standards version to 4.4.1, no changes needed.
  * Update standards version to 4.5.0, no changes needed.

  [ Jenkins ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

  [ Debian Janitor ]
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 04 Dec 2022 19:45:42 +0000

libhtml-defang-perl (1.07-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org.

  [ gregor herrmann ]
  * Update URLs from {search,www}.cpan.org to MetaCPAN.

  [ Jonas Smedegaard ]
  * Simplify rules.
    Stop build-depend on devscripts cdbs.
  * Stop build-depend on dh-buildinfo.
  * Declare compliance with Debian Policy 4.3.0.
  * Set Rules-Requires-Root: no.
  * Wrap and sort control file.
  * Update copyright info:
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
    + Use https protocol in Format URL.
    + Extend coverage of packaging.
    + Extend coverage for main upstream author.
  * Update git-buildpackage config: Filter any .git* file.
  * Update watch file:
    + Bump to file format 4.
    + Rewrite usage comment.
    + Use substitution strings.
  * Add lintian overrides regarding License-Reference.
  * Drop all patches: Regex syntax issues fixed upstream.
  * Bump debhelper compatibility level to 9.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 22 Feb 2019 15:54:31 +0100

libhtml-defang-perl (1.04-4) unstable; urgency=medium

  * Team upload

  * amend 0002-Fix-test-script-regexp-syntax-for-Perl-5.22.patch to also fix
    unescaped braces that are fatal for Perl 5.26
    (Closes: #826480)

 -- Damyan Ivanov <dmn@debian.org>  Thu, 22 Jun 2017 11:57:58 +0000

libhtml-defang-perl (1.04-3) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Niko Tyni ]
  * Declare the package autopkgtestable.
  * Fix regexp syntax for Perl 5.22, eliminating new warnings.
    (Closes: #809096)

 -- Niko Tyni <ntyni@debian.org>  Sun, 27 Dec 2015 18:57:12 +0200

libhtml-defang-perl (1.04-2) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to use canonical hostname (anonscm.debian.org).

  [ Jonas Smedegaard ]
  * Add README.source emphasizing control.in file as *not* a
    show-stopper for contributions, referring to wiki page for details.
  * Bump to standards-version 3.9.5.
  * Fix use canonical Vcs-Git URL.
  * Update watch file to use metacpan.org URL.
  * Update copyright info:
    + Extend coverage of packaging, and bump its licensing to GPL-3+.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 21 May 2014 16:37:50 +0200

libhtml-defang-perl (1.04-1) unstable; urgency=low

  * Friendly takeover (via Debian Perl Group).
    Thanks to Ivan for past contributions.
  * Repackage using CDBS.
    Build-depend on cdbs, devscripts and dh-buildinfo.
    Relax to build-depend unversioned on debhelper.
  * Bump standards-version to 3.9.3.
  * Bump debhelper compatibility level to 8.
  * Use dpkg source format 3.0 (quilt).
  * Add git-buildpackage config enabling pristine-tar and signed tags.
    Git-ignore quilt .pc subdir.
  * Shorten short description, and rewrap long description at 72 chars.
  * Add Vcs-* paragraphs.
  * Rewrite copyright file using format 1.0.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 19 Jul 2012 20:55:11 +0200

libhtml-defang-perl (1.02-1) unstable; urgency=low

  * Initial Release (closes: Bug#575285).

 -- Ivan Kohler <ivan-debian@420.am>  Wed, 24 Mar 2010 13:29:07 -0700
